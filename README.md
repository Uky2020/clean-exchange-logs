# Clean Exchange Logs

This script will help you clean all the logs on your Exchange server which can get pretty big.
The source script was edited with help of two sources:

https://social.technet.microsoft.com/wiki/contents/articles/31117.exchange-logging-clearing-log-files.aspx

and

https://gist.github.com/RippieUK/108a49239ae8572774af91b05cc8fafb

Changes:

- Added specific locations which are suggested on technet website (so it does not just recurse through whole "V15" folder),
- Added command to calculate disk size before and after,
- Added send to email quick report about free space.