#Edited/Created with help of two sources https://gallery.technet.microsoft.com/Clear-Exchange-2013-Log-71abba44 and https://gist.github.com/RippieUK/108a49239ae8572774af91b05cc8fafb by Uky

# Checks if Execution policy setting is different from "RemoteSigned" and if it is it changes it accordingly
$executionPolicy = Get-ExecutionPolicy
if ($executionPolicy -ne 'RemoteSigned') {
    Set-Executionpolicy RemoteSigned -Force
}

# Calculates free C:\ disk size before running the script
$FreeDiskBefore = Get-WmiObject Win32_LogicalDisk -Filter "DeviceID='C:'" 
$ResultBefore = $FreeDiskBefore.Freespace / 1GB

# This setting will tell the script which files/logs to keep, if it is set to "0" it will delete all logs, "7" means it will keep files younger than 7 days.
$days = 7

# Exact locations of logs you can delete based on the information provided on website: https://social.technet.microsoft.com/wiki/contents/articles/31117.exchange-logging-clearing-log-files.aspx, you can also add your own location, but you also need to add it on the bottom of the script.

$LoggingPath1 ="C:\Program Files\Microsoft\Exchange Server\V15\Logging\Diagnostics\DailyPerformanceLogs"
$LoggingPath2 ="C:\Program Files\Microsoft\Exchange Server\V15\Logging\Diagnostics\PerformanceLogsToBeProcessed"
$LoggingPath3 ="C:\Program Files\Microsoft\Exchange Server\V15\Bin\Search\Ceres\Diagnostics\ETLTraces"
$LoggingPath4 ="C:\Program Files\Microsoft\Exchange Server\V15\Bin\Search\Ceres\Diagnostics\Logs"
$LoggingPath5 ="C:\inetpub\logs\LogFiles"
$LoggingPath6 ="C:\Program Files\Microsoft\Exchange Server\V15\Logging\HttpProxy\RpcHttp" 

# Function to execute
Function CleanLogfiles($TargetFolder)
{
    Write-Host -ForegroundColor Yellow -BackgroundColor Black $TargetFolder

    if (Test-Path $TargetFolder) {
        $Now = Get-Date
        $LastWrite = $Now.AddDays(-$days)
        $Files = Get-ChildItem $TargetFolder -Recurse | Where-Object { $_.Extension -in '.log', '.blg', '.etl' -and $_.LastWriteTime -le $lastwrite } | Select-Object -ExpandProperty FullName  
        
        foreach ($File in $Files)
        {
            Write-Host "Deleting log file $File" -ForegroundColor "yellow";
            try {
                Remove-Item $File -ErrorAction Stop
            }
            catch {
                Write-Warning -Message $_.Exception.Message
            }
            
        }
    }
    else {
        Write-Host "The folder $TargetFolder doesn't exist! Check the folder path!" -ForegroundColor "red"
    }
}
# Function to delete content on the provided path variables. If you want, you can add your own.
CleanLogfiles($LoggingPath1)
CleanLogfiles($LoggingPath2)
CleanLogfiles($LoggingPath3)
CleanLogfiles($LoggingPath4)
CleanLogfiles($LoggingPath5)
CleanLogfiles($LoggingPath6)

# Calculates free C:\ disk size after running the script
$FreeDiskAfter = Get-WmiObject Win32_LogicalDisk -Filter "DeviceID='C:'" 
$ResultAfter = $FreeDiskAfter.Freespace / 1GB

# Dumps it into more readable form
$ResultFinished = "{0:N2} GB" -f ($ResultAfter - $ResultBefore)

# Sends email about free size on disk C:\ after cleaning up the logs

# Sender's address    
$FromAddress = "Report: Cleaning Exchange Logs <sender.email@domain.com>" 

# Recepient's address
$Recepients = "your.address@domain.com"

# Subject text
$Subject = "Exchange cleaning logs"

# Body message
$MessageUser = @"
<div>
<p style="font-size:14px;font-family:Arial">Cleaning up logs on your Exchange server was succesfull. Script managed to free <strong>$ResultFinished</strong> of disk space.</p>
<p style="font-size:14px;font-family:Arial">Best regards, your Cleaning Exchange Logs Script</p>
"@

# SMTP server
$SMTP = "your.smtp.server@domain.com"  

# Sending report to the address you specified before

$MailMessageAdmin = @{   
 
    From = $FromAddress
    To = $Recepients
    Subject = $Subject
    Body = $MessageUser
    SmtpServer = $SMTP        
}

Send-MailMessage @MailMessageAdmin -BodyAsHtml -Encoding UTF8